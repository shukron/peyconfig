# Peyconfig
_"Like `Kconfig`, only better, written in Python"_

## Goal
`Peyconfig` is a library that let you define configuration options for different
parts of your program.
It is heavily inspired by both [Linux Kernel's `Kconfig`](https://www.kernel.org/doc/Documentation/kbuild/kconfig-language.txt)
and PyPy's [config system](http://doc.pypy.org/en/latest/configuration.html).

### Why reinvent the wheel?
- PyPy's config system is not available as a standalone Python module, so you
can't really use it easily. Also it does not support loading configuration from
a file.
- Kconfig is awkward to use in Python code, and does not offer namespacing
(among other useful features)

## Features
So what do we want from a good configuration system?

1. **Structured:** Top-level components should be able to include configuration
definition from other components to create a tree-like structure.
This tree structure should be reflected at runtime to the program.
2. **Persistent:** The config system should be _able_ to save the user's
configuration, and load it at future invocations.
3. **Format independent:** The config system should enable developers to extend
it with support for saving and loading configuration files in multiple formats,
such as `JSON`, `ini` etc.
4. **Reliable:** The config system should ensure the validity of the
configuration values it is given, and enforce any dependencies defined in the
configuration definition.
5. **Feature-rich:** The config system should provide an extensive set of
configuration options. It should also provide the developer the ability to
extend this set of options.

### Configuration: Definition vs. Values
We distinguish between two different aspect of configuration:
1. The configuration's _definition_, which might include:
   1. What options are there.
   2. What are their possible values.
   3. What are their default values (if the have one).
   4. A textual description of the option.
   5. Dependency / relationship with other options.
2. The configuration _values_,